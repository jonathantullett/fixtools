package FIX;

#
# FIX.pm - provides an interface to translate fix tags to human-readable format.
# loads the dictionaries from http://www.quickfixengine.org/xml.html when
# necessary.

use warnings;
use strict;
use Data::Dumper;
use LWP::Simple;

use Exporter 'import';

our @EXPORT_OK = qw(fix_tag_explain fix_translate fix_fields_as_hash);

use constant QUICKFIX_BASE_URL => "http://www.quickfixengine.org";

my $fix_field_separator = '\ca';	# fix field separator
my $dictionary = {};		# fix dictionary

#
# fix_translate
# fix_translate does a search/replace of the translated fields and values and returns the
# new string with the original prefix and suffix still in place.
#
sub fix_translate {
	my($line) = shift;

	if($line =~ /(8=FIX.*10=\d+)/) {
		my $translate = fix_translate_fields($line);

		$line =~ s/8=FIX.*10=\d+$fix_field_separator/$translate/;
	}
	return $line;
}

sub fix_fields_as_hash {
	my($line) = shift;

	if($line =~ /(8=FIX.*10=\d+)/) {
		my @fields = fix_translate_fields($line);
		my $hash = {};
		for(@fields) {
			my($key,$value) = split /=/;
			$hash->{$key} = $value;
		}
		return $hash;
	}
	return {};
}

#
# get_fix_version
# parse the Version tag, or user input tag, and determine/return the fix dictionary
# key
#
sub get_fix_version {
	my $version = shift;

	$version =~ s/[\.]//g;
	$version =~ s/^FIX//g;
	$version = "FIX$version";

	$version =~ tr/[a-z]/[A-Z]/;
	return $version;
}
#
# fix_translate_fields
# fix_translate_fields extracts fix tags and returns an array of translated ones in
# ["key1=value1", "key2=value2", "keyn=valuen"] format for further processing, if wanted.
#
sub fix_translate_fields {
	my $line = shift;
	my @fields = ();
	if($line =~ /(8=(FIX.[\d\.]+).*10=\d+)/) {
		@fields = split($fix_field_separator, $1);

		my $version = get_fix_version($2);

		# load the necessary dictionary, if not already loaded
		load_dictionary($version) if !exists $dictionary->{$version};

		# provide pointer to the current dictionary for this line parse.
		my $cdict = $dictionary->{$version};
		for(my $i=0; $i < scalar @fields; $i++) {
			my($key, $value) = split /=/, $fields[$i];
			# if we have a mapping for this message type
			if(exists $cdict->{$key}) {
				if(defined @{ $cdict->{$key} }[1]) {
					$value = @{ $cdict->{$key} }[1]->{$value} if defined @{ $cdict->{$key} }[1]->{$value};
				}
				# we certainly have the header name:
				$key = @{ $cdict->{$key} }[0];
			}
			$fields[$i] = "$key=$value";
		}
	}
	return (wantarray ? @fields : join(" ", @fields));
}

#
# load_dictionary
# load_dictionary parses the xml files downloaded from 
sub load_dictionary {
	my $version = shift;
	#print "loading $version\n";

	$version = get_fix_version($version);

	return if exists $dictionary->{$version};

	my $xml_source = "/tmp/$version.xml";

	if(! -f $xml_source || !-s $xml_source) {
		my $s = get QUICKFIX_BASE_URL."/$version.xml";
		if(! $s) {
			print "Unable to load FIX dictionary for version '$version'.\n";
			print "Attempted to load from ".QUICKFIX_BASE_URL."/$version.xml\n";
			exit(1);
		}
		open(XMLW, "> $xml_source") || die "Unable to open cache $xml_source for writing: $!\n";
		print XMLW $s;
		close(XMLW);
	}
	my $curr_tag;
	my $curr_enum = {};
	open(XML, $xml_source) || die "Unable to locate dictionary for version '$version': $!\n";
	$dictionary->{$version} = {};
	my $cdict = $dictionary->{$version};
	while(<XML>) {
		if(/field number='(\d+)' name='(\S+)'/) {
			my $num = $1;
			my $name = $2;
			#   <field number='163' name='SettlInstTransType' type='CHAR'>
			if(scalar keys %{ $curr_enum }) {
				push @{ $cdict->{$curr_tag} }, $curr_enum;
				$curr_enum = {};
			}
			$curr_tag = $num;
			push @{ $cdict->{$curr_tag} }, $name;
		} elsif(/field name='(\S+)' number='(\d+)'/) {
			my $name = $1;
			my $num = $2;
			#   <field name='MaxMessageSize' number='383' type='LENGTH'/>
			if(scalar keys %{ $curr_enum }) {
				push @{ $cdict->{$curr_tag} }, $curr_enum;
				$curr_enum = {};
			}
			$curr_tag = $num;
			push @{ $cdict->{$curr_tag} }, $name;
		} elsif(/value enum='(\S+)' description='(\S+)'/ && $curr_tag) {
			my $enum = $1;
			my $desc = $2;
			#   <value enum='C' description='CANCEL' />
			$curr_enum->{$enum} = $desc;
		} elsif(/value description='(\S+)' enum='(\S+)'/ && $curr_tag) {
			my $desc = $1;
			my $enum = $2;
			#   <value enum='C' description='CANCEL' />
			$curr_enum->{$enum} = $desc;
		}
	}
	close(XML);
	# write the final enum dictionary for the current tag, if any.
	if(scalar keys %{ $curr_enum }) {
		push @{ $cdict->{$curr_tag} }, $curr_enum;
	}
}

sub fix_tag_explain {
	my $tag = shift;
	my $version = shift || 42;

	return unless $tag;

	$version = get_fix_version($version);
	load_dictionary($version);
	my $cdict = $dictionary->{$version};

	if(exists $cdict->{$tag}) {
		print "Tag: $tag => ".$cdict->{$tag}[0]."\n";
		if(defined $cdict->{$tag}[1]) {
			for(keys %{ $cdict->{$tag}[1] }) {
				print "   $_ => ".$cdict->{$tag}[1]->{$_}."\n";
			}
		} else {
		}
	}
}

1;
