package DDA::Order;

use warnings;
use strict;

sub new {
	my($class, $data) = @_;

	my $self = {
		ack => 0,
		debug => 0,
		};

	bless $self, $class;

	$self->load_data($data);

	return $self;
}

sub load_data {
	my($self, $data) = @_;

	foreach(qw(symbol oid side quantity remaining price)) {
		if(exists $data->{$_}) {
			$self->{$_} = $data->{$_};
		}
	}
	if(!exists $self->{'remaining'}) { $self->{'remaining'} = $self->{'quantity'}; }
}

sub ack {
	my $self = shift;
	print "order:".$self->{'oid'}." ack\n" if $self->{'debug'};
	$self->{'ack'} = 1;
}

sub fill {
	my($self, $q) = @_;
	print "order:".$self->{'oid'}." fill($q)\n" if $self->{'debug'};
	$self->{'remaining'} -= $q;
}

sub cancel {
	my($self, $q, $lq) = @_;
	print "order:".$self->{'oid'}." cancel($q, $lq)\n" if $self->{'debug'};
	if(!defined $lq) {
		$self->{'remaining'} -= $q;
	} else {
		$self->{'remaining'} = $lq;
	}
}

sub reject {
	my($self, $q) = @_;
	print "order:".$self->{'oid'}." reject($q)\n" if $self->{'debug'};
	$self->{'remaining'} = 0;
}

1;
